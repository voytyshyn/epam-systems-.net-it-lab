﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FuncCalc.BuiltinFuncs;
using FuncCalc.DesktopUI.Code;
using FuncCalc.Core;

namespace FuncCalc.DesktopUI
{
    public partial class CalcControl : UserControl
    {
        private IEnumerable<IFunc> _funcs;
        private IFunc _selectedFunc;

        public CalcControl()
        {
            InitializeComponent();
        }

        public IEnumerable<IFunc> Funcs
        {
            get { return this._funcs; }
            set
            {
                this._funcs = value;
                this.BindFuncs(this._funcs);
            }
        }

        private void BindFuncs(IEnumerable<IFunc> funcs)
        {
            cmbFunc.DataSource = funcs;
        }

        private void cmbFunc_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            this._selectedFunc = (IFunc)c.SelectedValue;
            lblDescription.Text = this._selectedFunc.Description;
            
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            double x = double.Parse(txtArgument.Text);
            double result = this._selectedFunc.Value(x);
            txtResult.Text = result.ToString();
        }

    }
}
