﻿namespace FuncCalc.DesktopUI
{
    partial class CalcControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbCalc = new System.Windows.Forms.GroupBox();
            this.cmbFunc = new System.Windows.Forms.ComboBox();
            this.txtArgument = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lnkReference = new System.Windows.Forms.LinkLabel();
            this.btnCalc = new System.Windows.Forms.Button();
            this.grbCalc.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbCalc
            // 
            this.grbCalc.Controls.Add(this.btnCalc);
            this.grbCalc.Controls.Add(this.lnkReference);
            this.grbCalc.Controls.Add(this.lblDescription);
            this.grbCalc.Controls.Add(this.txtResult);
            this.grbCalc.Controls.Add(this.txtArgument);
            this.grbCalc.Controls.Add(this.cmbFunc);
            this.grbCalc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbCalc.Location = new System.Drawing.Point(0, 0);
            this.grbCalc.Name = "grbCalc";
            this.grbCalc.Size = new System.Drawing.Size(335, 226);
            this.grbCalc.TabIndex = 1;
            this.grbCalc.TabStop = false;
            // 
            // cmbFunc
            // 
            this.cmbFunc.DisplayMember = "Title";
            this.cmbFunc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFunc.FormattingEnabled = true;
            this.cmbFunc.Location = new System.Drawing.Point(32, 34);
            this.cmbFunc.Name = "cmbFunc";
            this.cmbFunc.Size = new System.Drawing.Size(121, 21);
            this.cmbFunc.TabIndex = 0;
            this.cmbFunc.SelectedValueChanged += new System.EventHandler(this.cmbFunc_SelectedValueChanged);
            // 
            // txtArgument
            // 
            this.txtArgument.Location = new System.Drawing.Point(32, 78);
            this.txtArgument.Name = "txtArgument";
            this.txtArgument.Size = new System.Drawing.Size(121, 20);
            this.txtArgument.TabIndex = 1;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(32, 153);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(121, 20);
            this.txtResult.TabIndex = 2;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(204, 34);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(35, 13);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = "label1";
            // 
            // lnkReference
            // 
            this.lnkReference.AutoSize = true;
            this.lnkReference.Location = new System.Drawing.Point(204, 85);
            this.lnkReference.Name = "lnkReference";
            this.lnkReference.Size = new System.Drawing.Size(55, 13);
            this.lnkReference.TabIndex = 4;
            this.lnkReference.TabStop = true;
            this.lnkReference.Text = "linkLabel1";
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(32, 104);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(121, 23);
            this.btnCalc.TabIndex = 5;
            this.btnCalc.Text = "Calculate";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // CalcControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grbCalc);
            this.Name = "CalcControl";
            this.Size = new System.Drawing.Size(335, 226);
            this.grbCalc.ResumeLayout(false);
            this.grbCalc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbCalc;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TextBox txtArgument;
        private System.Windows.Forms.ComboBox cmbFunc;
        private System.Windows.Forms.LinkLabel lnkReference;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnCalc;
    }
}
