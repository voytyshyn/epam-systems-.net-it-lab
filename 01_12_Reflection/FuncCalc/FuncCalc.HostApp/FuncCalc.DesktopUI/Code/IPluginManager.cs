﻿using FuncCalc.BuiltinFuncs;
using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.DesktopUI.Code
{
    public interface IPluginManager
    {
        IEnumerable<IFunc> GetFuncs();
    }
}