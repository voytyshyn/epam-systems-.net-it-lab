﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuncCalc.DesktopUI.Code
{
    public class Logger
    {
        private static Logger instance = null;

        private static string fileName = null;
        private static object sync = new object();

        private Logger()
        {
            fileName = Path.Combine(Application.StartupPath, "calc-func-log.txt");
        }

        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = new Logger();
                        }
                    }
                }
                return instance;
            }
        }      

        public void Write(string message)
        {
            DateTime now = DateTime.Now;
            string text = string.Format("{0} - {1}", now, message);
            File.AppendAllLines(fileName, new string[] { text });
        }
    }
}
