﻿using FuncCalc.BuiltinFuncs;
using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.DesktopUI.Code
{
    public class PluginManager: IPluginManager
    {
        private readonly string _pluginFolder;

        public PluginManager(string pluginFolder)
        {
            this._pluginFolder = pluginFolder;
        }

        public IEnumerable<IFunc> GetFuncs()
        {
            IEnumerable<Assembly> plugins = this.LoadPluginAssemblies();
            List<IFunc> funcs = new List<IFunc>();
            foreach(var plugin in plugins)
            {
                Logger.Instance.Write(string.Format("Plugin [{0}] was loaded", plugin));
                IEnumerable<IFunc> pluginFuncs = ObtainFuncs(plugin);
                foreach (var func in pluginFuncs)
                {
                    Logger.Instance.Write(string.Format("Function '{0}' was loaded", func.Title));
                }
                funcs.AddRange(pluginFuncs);
            }
            return funcs;
        }

        #region Helpers

        private IEnumerable<Assembly> LoadPluginAssemblies()
        {
            string[] files = Directory.GetFiles(this._pluginFolder, "*.dll");
            List<Assembly> plugins = new List<Assembly>();
            foreach(var file in files)
            {
                Assembly plugin = Assembly.LoadFrom(file);
                plugins.Add(plugin);
            }
            return plugins;
        }

        private IEnumerable<IFunc> ObtainFuncs(Assembly plugin)
        {
            List<IFunc> funcs = new List<IFunc>();
            funcs.AddRange(GetIFuncImplementations(plugin));
            funcs.AddRange(GetMakedByFuncAttribute(plugin));
            return funcs;
        }

        private IEnumerable<IFunc> GetIFuncImplementations(Assembly plugin)
        {
            Type[] types = plugin.GetTypes();
            List<Type> funcTypes = new List<Type>();
            foreach (var type in types)
            {
                if (typeof(IFunc).IsAssignableFrom(type))
                {
                    funcTypes.Add(type);
                }
            }
            List<IFunc> funcs = new List<IFunc>();
            foreach (var funcType in funcTypes)
            {
                funcs.Add((IFunc)Activator.CreateInstance(funcType));
            }
            return funcs;
        }

        private IEnumerable<IFunc> GetMakedByFuncAttribute(Assembly plugin)
        {
            Type[] types = plugin.GetTypes();
            List<MethodInfo> funcMethods = new List<MethodInfo>();
            foreach (var type in types)
            {
                foreach (var method in type.GetMethods())
                {
                    if (Attribute.IsDefined(method, typeof(FuncAttribute)))
                    {
                        funcMethods.Add(method);
                    }
                }
            }
            List<IFunc> funcs = new List<IFunc>();
            foreach (var funcMethod in funcMethods)
            {
                funcs.Add(new StaticMethodFunc(funcMethod));
            }
            return funcs;
        }

        #endregion
    }
}
