﻿namespace FuncCalc.DesktopUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucCalc = new FuncCalc.DesktopUI.CalcControl();
            this.SuspendLayout();
            // 
            // ucCalc
            // 
            this.ucCalc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ucCalc.Funcs = null;
            this.ucCalc.Location = new System.Drawing.Point(94, 63);
            this.ucCalc.Name = "ucCalc";
            this.ucCalc.Size = new System.Drawing.Size(287, 183);
            this.ucCalc.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 308);
            this.Controls.Add(this.ucCalc);
            this.Name = "MainForm";
            this.Text = "Maths Funcs Calc";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CalcControl ucCalc;

    }
}

