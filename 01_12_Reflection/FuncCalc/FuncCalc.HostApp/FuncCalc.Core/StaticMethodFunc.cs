﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Core
{
    public class StaticMethodFunc: IFunc
    {

        private readonly MethodInfo _staticMethod;
        private readonly FuncAttribute _funcAttribute;

        public StaticMethodFunc(MethodInfo staticMethod)
        {
            #region Validation

            if (staticMethod == null)
            {
                throw new ArgumentNullException("'staticMethod' should not be null");
            }

            if (staticMethod.IsStatic != true)
            {
                throw new ArgumentException("The method should be static");
            }

            if (staticMethod.ReturnType != typeof(double))
            {
                throw new ArgumentException("The method should return 'double'");
            }

            if ((staticMethod.GetParameters().Length == 1 && staticMethod.GetParameters()[0].ParameterType == typeof(double)) == false)
            {
                throw new ArgumentException("The method should take only 1 'double' argument");
            }

            #endregion

            this._funcAttribute = staticMethod.GetCustomAttribute<FuncAttribute>();

            if (this._funcAttribute == null)
            {
                throw new ArgumentException("The method is not marked with the 'FuncAttribute'");
            }

            this._staticMethod = staticMethod;
        }

        public double Value(double x)
        {
            return (double)this._staticMethod.Invoke(null, new object[] { x });
        }

        public string Description
        {
            get { return this._funcAttribute.Description; }
        }

        public string Url
        {
            get { return this.Url; }
        }

        public string Title
        {
            get { return this._funcAttribute.Title; }
        }
    }
}
