﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Core
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public class FuncAttribute: Attribute
    {
        public string Description { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
    }
}