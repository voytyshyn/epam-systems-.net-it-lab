﻿using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Plugins.Pows
{
    public static class PowFuncs
    {
        [Func(Title = "x^2", Description = "sqr", Url = "")]
        public static double Sqr(double x)
        {
            return x * x;
        }

        [Func(Title = "x^3", Description = "x^3", Url = "")]
        public static double Pow3(double x)
        {
            return x * x * x;
        }
    }
}
