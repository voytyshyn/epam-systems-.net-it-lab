﻿using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Plugins.Logs
{
    public class LnFunc: IFunc
    {
        public string Description
        {
            get { return "ln(x)"; }
        }

        public string Title
        {
            get { return "ln(x)"; }
        }

        public string Url
        {
            get { return ""; }
        }

        public double Value(double x)
        {
            return Math.Log(x);
        }
    }
}
