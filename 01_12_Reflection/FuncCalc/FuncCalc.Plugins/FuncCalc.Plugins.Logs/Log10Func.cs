﻿using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Plugins.Logs
{
    public class Log10Func: IFunc
    {
        public string Description
        {
            get { return "lg(x)"; }
        }

        public string Title
        {
            get { return "lg(x)"; }
        }

        public string Url
        {
            get { return ""; }
        }

        public double Value(double x)
        {
            return Math.Log10(x);
        }
    }
}
