﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.Core
{
    public interface IFunc
    {
        double Value(double x);
        string Description { get; }
        string Url { get; }
        string Title { get; }
    }
}
