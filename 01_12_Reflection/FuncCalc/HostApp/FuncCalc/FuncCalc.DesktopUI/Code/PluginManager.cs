﻿using FuncCalc.BuiltinFuncs;
using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.DesktopUI.Code
{
    public class PluginManager: IPluginManager
    {
        private readonly string _pluginFolder;

        public PluginManager(string pluginFolder)
        {
            this._pluginFolder = pluginFolder;
        }

        public IEnumerable<IFunc> GetFuncs()
        {
            IEnumerable<Assembly> plugins = this.LoadPluginAssemblies();
            List<IFunc> funcs = new List<IFunc>();
            foreach(var plugin in plugins)
            {
                funcs.AddRange(ObtainFuncs(plugin));
            }
            return funcs;
        }

        #region Helpers

        private IEnumerable<Assembly> LoadPluginAssemblies()
        {
            string[] files = Directory.GetFiles(this._pluginFolder, "*.dll");
            List<Assembly> plugins = new List<Assembly>();
            foreach(var file in files)
            {
                Assembly plugin = Assembly.LoadFrom(file);
                plugins.Add(plugin);
            }
            return plugins;
        }

        private IEnumerable<IFunc> ObtainFuncs(Assembly plugin)
        {
            Type[] types = plugin.GetTypes();
            List<Type> funcTypes = new List<Type>();
            foreach(var type in types)
            {
                if (typeof(IFunc).IsAssignableFrom(type))
                {
                    funcTypes.Add(type);
                }
            }
            List<IFunc> funcs = new List<IFunc>();
            foreach(var funcType in funcTypes)
            {
                funcs.Add((IFunc)Activator.CreateInstance(funcType));
            }
            return funcs;
        }

        #endregion
    }
}
