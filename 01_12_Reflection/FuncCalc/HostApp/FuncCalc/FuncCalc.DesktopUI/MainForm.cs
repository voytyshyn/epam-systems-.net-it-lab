﻿using FuncCalc.BuiltinFuncs;
using FuncCalc.Core;
using FuncCalc.DesktopUI.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuncCalc.DesktopUI
{
    public partial class MainForm : Form
    {
        private readonly IPluginManager _pluginManager;

        public MainForm()
        {
            string relativePluginFolder = ConfigurationManager.AppSettings["pluginFolder"];
            string pluginFolder = Path.Combine(Application.StartupPath, relativePluginFolder);
            this._pluginManager = new PluginManager(pluginFolder);
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            List<IFunc> funcs = new List<IFunc>();
            funcs.Add(new SinFunc());
            funcs.Add(new CosFunc());

            var pluginFuncs = this._pluginManager.GetFuncs();

            funcs.AddRange(pluginFuncs);

            ucCalc.Funcs = funcs;
        }
    }
}
