﻿using FuncCalc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncCalc.BuiltinFuncs
{
    public class SinFunc: IFunc
    {
        public double Value(double x)
        {
            return Math.Sin(x);
        }

        public string Description
        {
            get { return "sin(x)"; }
        }

        public string Url
        {
            get { return "http://en.wikipedia.org/wiki/Trigonometric_functions#Sine.2C_cosine_and_tangent"; }
        }

        public string Title
        {
            get { return "sin(x)"; }
        }
    }
}
