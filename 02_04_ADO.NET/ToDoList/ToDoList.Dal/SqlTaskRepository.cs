﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ToDoList.Entities;

namespace ToDoList.Dal
{
    public class SqlTaskRepository: ITaskRepository
    {
        #region Queries

        private const string GET_ALL_QUERY = "SELECT [Id],[Title],[IsDone] FROM [dbo].[Task]";
        private const string GET_BY_IS_DONE_QUERY = "SELECT [Id],[Title],[IsDone] FROM [dbo].[Task] WHERE [IsDone] = @isDone";
        private const string GET_BY_ID_QUERY = "SELECT [Id],[Title],[IsDone] FROM [dbo].[Task] WHERE [Id] = @id";
        private const string INSERT_QUERY = "INSERT INTO [dbo].[Task] ([Title],[IsDone]) VALUES (@title, @isDone); SELECT CAST(SCOPE_IDENTITY() AS INT);";
        private const string UPDATE_QUERY = "UPDATE [dbo].[Task] SET [Title] = @title, [IsDone] = @isDone WHERE [Id] = @id";
        private const string DELETE_QUERY = "DELETE FROM [dbo].[Task] WHERE [Id] = @id";

        #endregion

        #region Fiedls

        private readonly string _connectionString;

        #endregion

        #region Constructors

        public SqlTaskRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region ITaskRepository

        public List<Task> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(GET_ALL_QUERY, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<Task> tasks = new List<Task>();

                        while (reader.Read())
                        {
                            Task task = ReadTask(reader);
                            tasks.Add(task);
                        }

                        return tasks;
                    }
                }
            }
        }

        public List<Task> GetByIsDone(bool isDone)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(GET_BY_IS_DONE_QUERY, connection))
                {
                    command.Parameters.AddWithValue("isDone", isDone);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<Task> tasks = new List<Task>();

                        while (reader.Read())
                        {
                            Task task = ReadTask(reader);
                            tasks.Add(task);
                        }

                        return tasks;
                    }
                }
            }
        }

        public Task GetById(int id)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(GET_BY_ID_QUERY, connection))
                {
                    command.Parameters.AddWithValue("id", id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return this.ReadTask(reader);
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
            }
        }

        public Task Add(Task task)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(INSERT_QUERY, connection))
                {
                    command.Parameters.AddWithValue("title", task.Title);
                    command.Parameters.AddWithValue("isDone", task.IsDone);

                    int taskId = (int)command.ExecuteScalar();
                    task.Id = taskId;

                    return task;
                }
            }
        }

        public void Update(Task task)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(UPDATE_QUERY, connection))
                {
                    command.Parameters.AddWithValue("id", task.Id);
                    command.Parameters.AddWithValue("title", task.Title);
                    command.Parameters.AddWithValue("isDone", task.IsDone);

                    command.ExecuteNonQuery();

                }
            }
        }

        public void Delete(int taskId)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(DELETE_QUERY, connection))
                {
                    command.Parameters.AddWithValue("id", taskId);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Helpers

        private Task ReadTask(SqlDataReader reader)
        {
            Task task = new Task();
            task.Id = (int)reader["Id"];
            task.Title = (string)reader["Title"];
            task.IsDone = (bool)reader["IsDone"];
            return task;
        }

        #endregion


    }
}
