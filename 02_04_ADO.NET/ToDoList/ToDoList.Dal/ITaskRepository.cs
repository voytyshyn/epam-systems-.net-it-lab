﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Entities;

namespace ToDoList.Dal
{
    public interface ITaskRepository
    {
        List<Task> GetAll();
        List<Task> GetByIsDone(bool isDone);
        Task GetById(int id);

        Task Add(Task task);
        void Update(Task task);
        void Delete(int taskId);

    }
}
