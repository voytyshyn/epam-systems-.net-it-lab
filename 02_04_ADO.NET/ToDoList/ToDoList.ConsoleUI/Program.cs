﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ToDoList.Dal;
using ToDoList.Entities;

namespace ToDoList.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ToDoList"].ConnectionString;
            ITaskRepository taskRepository = new SqlTaskRepository(connectionString);
            List<Task> tasks = taskRepository.GetAll();
            List<Task> completedTasks = taskRepository.GetByIsDone(true);
            List<Task> pendingTasks = taskRepository.GetByIsDone(false);
            Task task2 = taskRepository.GetById(2);

            Task task4 = taskRepository.Add(new Task { Title = "Task 4", IsDone = false });
            taskRepository.Update(new Task { Id = 4, Title = "Task 4 (new)", IsDone = true });
            Task task4_1 = taskRepository.GetById(4);

            taskRepository.Delete(4);
            Task task4_2 = taskRepository.GetById(4);               
        }
    }
}
