SET IDENTITY_INSERT Task ON

insert into Task 
		(Id, Title, IsDone)
	values
		(1, 'Task 1', 1),
		(2, 'Task 2', 0),
		(3, 'Task 3', 0);
		
SET IDENTITY_INSERT Task OFF		