﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoList.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }
    }
}
