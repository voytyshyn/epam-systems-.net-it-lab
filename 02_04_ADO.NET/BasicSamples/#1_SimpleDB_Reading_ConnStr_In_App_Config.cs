<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <connectionStrings>
    <add name="TestDB"
         providerName="System.Data.SqlClient"
         connectionString="server=EPUALVIL0079;integrated security=SSPI;database=Test DB" />
  </connectionStrings>
</configuration>



private static string GetDatabaseConnectionStr(string connStrSectionName)
{
  ConnectionStringSettings settings =
	ConfigurationManager.ConnectionStrings[connStrSectionName];

  return settings.ConnectionString;
}


// "ExecuteScalar" usage
static void Main(string[] args)
{
	  SqlConnection testConn = new SqlConnection(GetDatabaseConnectionStr("TestDB"));
	  SqlCommand testCommand = new SqlCommand("select count(*) from [Table_1]", testConn);
	  
	  testConn.Open();
	  
	  int objResult = (int)testCommand.ExecuteScalar();

	  testConn.Close();
	  
	  Console.WriteLine(objResult);          
}


// "ExecuteNonQuery" usage
static void Main(string[] args)
{

  string updateCommStr = "UPDATE [Table_1] " +
			  "SET [sdfsdf]  = 'aaa' " +
			  "WHERE [sdfsdf] = 'kk'";

	  SqlConnection testConn = new SqlConnection(GetDatabaseConnectionStr("TestDB"));
	  SqlCommand testCommand = new SqlCommand(updateCommStr, testConn);
	  
	  testConn.Open();

	  int rowsReturned = testCommand.ExecuteNonQuery();

	  testConn.Close();

	  Console.WriteLine("{0} rows returned.", rowsReturned);
  
}

// "ExecuteReader" usage
static void Main(string[] args)
{
	string selectCommStr = "select * from [Table_1]";

	SqlConnection testConn = new SqlConnection(GetDatabaseConnectionStr("TestDB"));
	testConn.Open();

	SqlCommand cmd = new SqlCommand(selectCommStr, testConn);
	SqlDataReader reader = cmd.ExecuteReader();

	while (reader.Read())
	{
	  Console.WriteLine(reader["sdfsdf"]);
	}          
}

// Reader with parameters
static void Main(string[] args)
{
  string inputMyParam = "aaa";

  SqlDataReader reader = null;
  SqlConnection testConn = null;
  
  try
  {
	  testConn = new SqlConnection(GetDatabaseConnectionStr("TestDB"));
	  testConn.Open();

	  // 1. declare command object with parameter
	  SqlCommand cmd = new SqlCommand(
		  "select * from [Table_1] where [sdfsdf] = @MyParam", testConn);

	  // 2. define parameters used in command object
	  SqlParameter param = new SqlParameter();
	  param.ParameterName = "@MyParam";
	  param.Value = inputMyParam;

	  // 3. add new parameter to command object
	  cmd.Parameters.Add(param);

	  // get data stream
	  reader = cmd.ExecuteReader();

	  // write each record
	  while (reader.Read())
	  {
		  Console.WriteLine(reader["sdfsdf"]);
	  }
  }
  finally
{
	// close reader
	if (reader != null)
	{
		reader.Close();
	}

	// close connection
	if (testConn != null)
	{
		testConn.Close();
	}
}
}