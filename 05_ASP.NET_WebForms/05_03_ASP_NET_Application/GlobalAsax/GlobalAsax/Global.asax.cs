﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace GlobalAsax
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Debug.WriteLine("Application_Start");
        }       
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_BeginRequest: " + HttpContext.Current.Request.RawUrl);
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_AuthenticateRequest");
        }
        protected void Application_AuthorizeRequest(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_AuthorizeRequest");
        }
        protected void Application_ResolveRequestCache(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_ResolveRequestCache");
        }
        protected void Session_Start(Object sender, EventArgs e)
        {
            Debug.WriteLine("Session_Start");
        }        
        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_AcquireRequestState");
        }
        protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_PreRequestHandlerExecute");
        }
        protected void Application_PostRequestHandlerExecute(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_PostRequestHandlerExecute");
        }
        protected void Application_ReleaseRequestState(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_ReleaseRequestState");
        }
        protected void Application_UpdateRequestCache(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_UpdateRequestCache");
        }
        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_EndRequest");
        }
        protected void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_PreSendRequestHeaders");
        }
        protected void Application_PreSendRequestContent(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_PreSendRequestContent");
        }
        protected void Session_End(Object sender, EventArgs e)
        {            
            Debug.WriteLine("Session_End");
        }
        protected void Application_Disposed(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_Disposed");
        }
        protected void Application_End(Object sender, EventArgs e)
        {
            Debug.WriteLine("Application_End");
        }
    }
}