create database todolist;

go

use todolist;

go
create table Priority
(
	Id int not null identity(1,1),
	Name nvarchar(50) not null,
	Wage int not null
);

go

alter table Priority add constraint PK_Priority_Id primary key (Id);

go

create table Task
(
	Id int not null identity(1,1),
	PriorityId int null,
	Title nvarchar(256) not null,
	Description nvarchar(1024) null,
	DueDate datetime null,
	IsDone bit not null
);

go

alter table Task add constraint PK_Task_Id primary key (Id);

go

alter table Task add constraint FK_Task_PriorityId_Priority_Id foreign key (PriorityId) references Priority(Id);

go

create table [User]
(
	Id int not null identity(1,1),
	FirstName nvarchar(255) not null,
	Surname nvarchar(255) not null,
	Login nvarchar(50) not null,
	Password nvarchar(50) not null,
	IsAdmint bit not null
);

go

alter table [User] add constraint PK_User_Id primary key (Id);

go 

alter table [User] add constraint UQ_User_Login unique (Login);



