use todolist;

insert into Priority (Name, Wage) 
	values 
		('Critical', 5),
		('High', 4),
		('Normal', 3),
		('Low', 2),
		('Very Low', 1)
		
INSERT INTO [dbo].[Task]
           ([PriorityId],[Title],[IsDone])
     VALUES
           (3, 'Create database', 1),
           (3, 'Implement reposiroty', 0),
           (3, 'Configure an object data source', 0),
           (3, 'Configure GridView', 0)

INSERT INTO [dbo].[User]
           ([FirstName],[Surname],[Login],[Password],[IsAdmint])
     VALUES
           ('Volodymyr', 'Voityshyn', 'voityshyn', '123456', 1);           
GO


GO

		