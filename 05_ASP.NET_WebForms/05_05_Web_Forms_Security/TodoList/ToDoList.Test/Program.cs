﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Text;
using TodoList.Entities;


namespace ToDoList.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["vvvToDoListEntities"].ConnectionString;

            Console.WriteLine("Tasks: ");
            using (ObjectContext context = new ObjectContext(connectionString))
            {
                List<Task> tasks = context
                                        .CreateObjectSet<Task>()
                                        .Include("Priority")
                                        .ToList();
                foreach (var task in tasks)
                {
                    Console.WriteLine("{0} {1} {2}", task.Id, task.Title, (task.Priority != null ? task.Priority.Name : "undefined"));
                }
            }

            Console.WriteLine("Priorities: ");
            using (ObjectContext context = new ObjectContext(connectionString))
            {
                List<Priority> priorities = context.CreateObjectSet<Priority>().ToList();
                foreach (var priority in priorities)
                {
                    Console.WriteLine("{0} {1}", priority.Id, priority.Name);
                }
            }
            Console.ReadKey();
        }
    }
}
