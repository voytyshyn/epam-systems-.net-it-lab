﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TodoList.WebUI.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Editing Task</h1>

    <asp:ObjectDataSource ID="odsTask" runat="server"
        OnObjectCreating="odsTask_ObjectCreating"
        DataObjectTypeName="TodoList.Entities.Task"
        TypeName="TodoList.Dal.EFTaskRepository"
        SelectMethod="GetById"
        UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="task-id" Name="id" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:FormView ID="frvTask" runat="server"
        DefaultMode="Edit"
        DataSourceID="odsTask"
        DataKeyNames="ID"
        OnItemUpdated="frvTask_ItemUpdated">        
        <EditItemTemplate>

            <table>
                <tbody>
                    <tr>
                        <th>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </th>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                Display="Dynamic"
                                ErrorMessage="Required Field"/>
                            <asp:RegularExpressionValidator ID="revTitle" runat="server" ControlToValidate="txtTitle" 
                                ValidationExpression=".{1,255}"
                                Display="Dynamic"
                                ErrorMessage="Length should not be greater than 255" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <asp:Label ID="lblIsDone" runat="server" Text="Is Done"></asp:Label>
                        </th>
                        <td>
                            <asp:CheckBox ID="chkIsDone" runat="server" Checked='<%# Bind("IsDone") %>' />
                        </td>
                    </tr>
                </tbody>                
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Update" CausesValidation="true" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                        </td>
                    </tr>
                </tfoot>
            </table>                                  
            
        </EditItemTemplate>
    </asp:FormView>


</asp:Content>
