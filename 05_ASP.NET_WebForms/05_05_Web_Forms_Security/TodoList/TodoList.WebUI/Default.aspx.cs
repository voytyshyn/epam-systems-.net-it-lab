﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TodoList.Dal;

namespace TodoList.WebUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void odsTask_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["todolistEntities"].ConnectionString;
            e.ObjectInstance = new EFTaskRepository(connectionString);
        }

        
    }
}