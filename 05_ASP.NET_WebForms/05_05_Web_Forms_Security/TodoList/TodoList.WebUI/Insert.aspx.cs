﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TodoList.Dal;

namespace TodoList.WebUI
{
    public partial class Insert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void frvTask_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void odsTask_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["vvvToDoListEntities"].ConnectionString;
            e.ObjectInstance = new EFTaskRepository(connectionString);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}