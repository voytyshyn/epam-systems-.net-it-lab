﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Insert.aspx.cs" Inherits="TodoList.WebUI.Insert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>New Task</h1>

    <asp:ObjectDataSource ID="odsTask" runat="server"
        OnObjectCreating="odsTask_ObjectCreating"
        DataObjectTypeName="TodoList.Entities.Task"
        TypeName="TodoList.Dal.EFTaskRepository"        
        InsertMethod="Insert">        
    </asp:ObjectDataSource>

    <asp:FormView ID="frvTask" runat="server"
        DefaultMode="Insert"
        DataSourceID="odsTask"
        DataKeyNames="ID"
        OnItemInserted="frvTask_ItemInserted">        
        <InsertItemTemplate>

            <table>
                <tbody>
                    <tr>
                        <th>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </th>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                Display="Dynamic"
                                ErrorMessage="Required Field"/>
                            <asp:RegularExpressionValidator ID="revTitle" runat="server" ControlToValidate="txtTitle" 
                                ValidationExpression=".{1,255}"
                                Display="Dynamic"
                                ErrorMessage="Length should not be greater than 255" />
                        </td>
                    </tr>                    
                </tbody>                
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Insert" CausesValidation="true" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                        </td>
                    </tr>
                </tfoot>
            </table>                                  
            
        </InsertItemTemplate>
    </asp:FormView>
</asp:Content>
