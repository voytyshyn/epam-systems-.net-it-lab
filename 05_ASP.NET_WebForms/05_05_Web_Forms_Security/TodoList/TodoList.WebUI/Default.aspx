﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TodoList.WebUI.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>To Do List</h1>

    <asp:ObjectDataSource ID="odsTask" runat="server"
        OnObjectCreating="odsTask_ObjectCreating"
        DataObjectTypeName="TodoList.Entities.Task"
        TypeName="TodoList.Dal.EFTaskRepository"
        SelectMethod="Get"
        DeleteMethod="Delete">
    </asp:ObjectDataSource>

    <asp:LinkButton ID="lnbNew" runat="server" PostBackUrl="~/Insert.aspx">New</asp:LinkButton>

    <asp:GridView ID="grvTask" runat="server"
        DataSourceID="odsTask"
        AutoGenerateColumns="false"
        DataKeyNames="Id"
        ShowHeader="true">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>Done</HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkDone" runat="server" Checked='<%# Eval("IsDone") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>Title</HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" NavigateUrl='<%# "~/Edit.aspx?task-id=" + Eval("Id") %>'>Edit</asp:HyperLink>
                    <asp:LinkButton ID="lbtDelete" runat="server" CommandName="Delete" OnClientClick="return confirm('Do you want to delete the task?');">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>
