﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoList.Entities;


namespace TodoList.Dal
{
    public interface ITaskRepository
    {
        List<Task> Get();
        Task GetById(int id);
        void Delete(Task task);        
        Task Insert(Task task);
        Task Update(Task task);
    }
}
