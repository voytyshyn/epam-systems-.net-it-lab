﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using TodoList.Entities;

namespace TodoList.Dal
{
    public class EFTaskRepository : ITaskRepository
    {
        private readonly string _connectionString;

        public EFTaskRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public List<Task> Get()
        {
            using (ObjectContext context = new ObjectContext(this._connectionString))
            {
                return context.CreateObjectSet<Task>().ToList();
            }
        }

        public void Delete(Task task)
        {
            using (ObjectContext context = new ObjectContext(this._connectionString))
            {
                Task t = context.CreateObjectSet<Task>().Single(r => r.Id == task.Id);
                context.DeleteObject(t);
                context.SaveChanges();
            }

        }

        public Task GetById(int id)
        {
            using (ObjectContext context = new ObjectContext(this._connectionString))
            {
                return context.CreateObjectSet<Task>().Single(r => r.Id == id);
            }
        }

        public Task Update(Task task)
        {
            using (ObjectContext context = new ObjectContext(this._connectionString))
            {
                context.CreateObjectSet<Task>().Attach(task);
                context.ObjectStateManager.ChangeObjectState(task, System.Data.EntityState.Modified);
                context.SaveChanges();
                return task;
            }
        }

        public Task Insert(Task task)
        {
            using (ObjectContext context = new ObjectContext(this._connectionString))
            {
                context.CreateObjectSet<Task>().AddObject(task);
                context.SaveChanges();
                return task;
            }
        }

    }
}
