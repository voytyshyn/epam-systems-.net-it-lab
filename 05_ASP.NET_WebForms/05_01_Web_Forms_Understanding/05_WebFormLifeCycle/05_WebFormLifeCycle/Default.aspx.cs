﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace _05_WebFormLifeCycle
{

    /*
		GET:
			1. Init
				- created all server control according to *.aspx
				- the controls have not been initialized
			2. Load
			3. Page_PreRender
			4. Serialization of view state
			5. Render
			6. Page_Unload
        
        POST:
      		1. Init
				- created all server control according to *.aspx
				- the controls have not been initialized
            2. Load
                - deserialized view state
                - applied control state
            3. Event Handlers
			3. Page_PreRender
			4. Serialization of view state
			5. Render
			6. Page_Unload
            
     */
    public partial class Default : System.Web.UI.Page
    {
        #region

        private const string POST_BACK_COUNT_KEY = "PostBackCount";

        #endregion


        #region Event Handlers

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_PreInit - " + GetTraceMessage());           
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_Init - " + GetTraceMessage());
        }

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_InitComplete - " + GetTraceMessage());
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_PreLoad - " + GetTraceMessage());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_Load - " + GetTraceMessage());
            if (IsPostBack == true)
            {
                this.PostBackCount += 1;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "btnSubmit_Click");

            txtText.BackColor = Color.Yellow;
            txtText.ForeColor = Color.Blue;
        }

        protected void btnSecondButton_Click(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "btnSecondButton_Click");
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_LoadComplete - " + GetTraceMessage());           
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_PreRender - " + GetTraceMessage());   
            ltrPostbacks.Text = this.PostBackCount.ToString();
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_PreRenderComplete - " + GetTraceMessage());
        }

        protected void Page_SaveState(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_SaveState - " + GetTraceMessage());
        }

        protected void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_SaveStateComplete - " + GetTraceMessage());
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            Trace.Warn("Custom Tracing", "Page_Unload - " + GetTraceMessage());
        }

        #endregion

        #region Private Properties

        private int PostBackCount
        {
            get
            {
                return (ViewState[POST_BACK_COUNT_KEY] != null)
                    ? (int)ViewState[POST_BACK_COUNT_KEY]
                    : 0;
            }
            set
            {
                ViewState[POST_BACK_COUNT_KEY] = value;
            }
        }

        #endregion

        #region Helpers

        private string GetTraceMessage()
        {
            object postBackCount = ViewState[POST_BACK_COUNT_KEY];
            string text = txtText.Text;
            string secondText = txtSecondText.Text;
            string message = string.Format("PostBack count: {0}; Text: {1}; Background: {2}; ForeColor: {3}; Second Text: {4}"
                , postBackCount
                , text
                , txtText.BackColor
                , txtText.ForeColor
                , secondText);
            return message;
        }

        #endregion


    }
}