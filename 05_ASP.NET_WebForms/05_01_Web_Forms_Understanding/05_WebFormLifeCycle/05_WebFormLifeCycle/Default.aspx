﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_05_WebFormLifeCycle.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding:40px;">
        <div>
            Postbacks count: <asp:Literal ID="ltrPostbacks" runat="server" />
        </div>
        <div>
            <asp:TextBox ID="txtText" runat="server" Text="This is a text"/>
        </div>
        <div>
            <asp:TextBox ID="txtSecondText" runat="server" Text=""/>
        </div>
        <div>
            <asp:Button ID="btnSubmit" runat="server" Text="Click to Change State" OnClick="btnSubmit_Click" />
            <asp:Button ID="btnSecondButton" runat="server" Text="Just Click" OnClick="btnSecondButton_Click" />
        </div>
    </div>
    </form>
</body>
</html>
