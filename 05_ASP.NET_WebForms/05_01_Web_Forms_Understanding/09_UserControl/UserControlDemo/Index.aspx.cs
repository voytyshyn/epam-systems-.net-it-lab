﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UserControlDemo
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gallery.Images = new List<string> 
            { 
                "~/Images/Image1.gif",
                "~/Images/Image2.gif",
                "~/Images/Image3.gif",
                "~/Images/Image4.gif"
            };
            gallery.OnCurrentImageChanged += gallery_OnCurrentImageChanged;
        }

        protected void gallery_OnCurrentImageChanged(object sender, EventArgs e)
        {

        }
    }
}