﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UserControlDemo
{
    public partial class Gallery : System.Web.UI.UserControl
    {
        private List<string> _images;

        public event EventHandler OnCurrentImageChanged;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["CurrentImageIndex"] == null)
            {
                CurrentImageIndex = 1;
            }
        }

        public int CurrentImageIndex
        {
            get 
            { 
                return (int)ViewState["CurrentImageIndex"]; 
            }
            set 
            {                
                if (ViewState["CurrentImageIndex"] != null && ((int)ViewState["CurrentImageIndex"]) != value)
                {
                    FireCurrentImageChanged();
                }
                ViewState["CurrentImageIndex"] = value;
            }
        }


        public List<string> Images
        {
            get { return _images; }
            set { _images = value; }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            this.CurrentImageIndex = (this.CurrentImageIndex <= 1 ? this._images.Count : this.CurrentImageIndex - 1);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            this.CurrentImageIndex = (this.CurrentImageIndex >= this._images.Count ? 1 : this.CurrentImageIndex + 1);

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblCurrentImageIndex.Text = this.CurrentImageIndex.ToString();
            lblImagesCount.Text = this._images.Count.ToString();
            imgPicture.ImageUrl = this._images[this.CurrentImageIndex - 1];
        }

        private void FireCurrentImageChanged()
        {
            if (this.OnCurrentImageChanged != null)
            {
                this.OnCurrentImageChanged(this, EventArgs.Empty);
            }
        }

    }
}