﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="UserControlDemo.Index" %>

<%@ Register Src="~/Gallery.ascx" TagPrefix="uc" TagName="Gallery" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="form1" runat="server">
    <div>
        <uc:Gallery runat="server" ID="gallery" CurrentImageIndex="2" OnCurrentImageChanged="gallery_OnCurrentImageChanged" />
    </div>
    </form>
</body>
</html>
