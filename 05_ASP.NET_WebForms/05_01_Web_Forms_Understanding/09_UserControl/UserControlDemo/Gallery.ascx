﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Gallery.ascx.cs" Inherits="UserControlDemo.Gallery" %>

<div>
    <asp:Image ID="imgPicture" runat="server" />    
</div>
<div>
    <asp:Button ID="btnPrev" runat="server" Text="<<" OnClick="btnPrev_Click" />
    &nbsp;
    <span>
        <asp:Label ID="lblCurrentImageIndex" runat="server" Text=""></asp:Label>
        &nbsp;
        <asp:Label ID="lblImagesCount" runat="server" Text=""></asp:Label>
    </span>
    &nbsp;
    <asp:Button ID="btnNext" runat="server" Text=">>" OnClick="btnNext_Click" />
</div>