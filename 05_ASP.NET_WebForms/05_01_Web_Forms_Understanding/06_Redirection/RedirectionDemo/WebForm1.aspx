﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="RedirectionDemo.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Page #1</h1>
        <div>
            <asp:HyperLink ID="hplGoToPage2" runat="server" NavigateUrl="~/WebForm2.aspx?color=red&size=12">Go To Page #2</asp:HyperLink>
        </div>
        <div>
            <asp:Button ID="btnGoToPage2" runat="server" Text="Go To Page #2" OnClick="btnGoToPage2_Click" />
        </div>
    </div>
    </form>
</body>
</html>
