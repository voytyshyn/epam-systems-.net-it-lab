﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RedirectionDemo
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string color = this.Request.QueryString["color"];
            string size = this.Request.QueryString["size"];
        }

        protected void tbtGoToPage1_Click(object sender, EventArgs e)
        {
            Server.Transfer("~/WebForm1.aspx");
        }
    }
}