﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="RedirectionDemo.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Page #2</h1>
        <asp:Button ID="tbtGoToPage1" runat="server" Text="Go To Page #1"  OnClick="tbtGoToPage1_Click"/>
    </div>
        
    </form>
</body>
</html>
