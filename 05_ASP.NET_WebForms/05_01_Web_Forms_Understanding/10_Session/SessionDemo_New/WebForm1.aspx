﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="SessionDemo_New.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:Label ID="lblSessionData" runat="server" Text="Enter Session Data:"></asp:Label>
            <asp:TextBox ID="txtSessionData" runat="server"></asp:TextBox>
        </div>
        <div>            
            <asp:Button ID="btnStoreData" runat="server" Text="Store Data" OnClick="btnStoreData_Click" />
            <asp:Button ID="btnRemoveData" runat="server" Text="Remove Data" OnClick="btnRemoveData_Click" />
        </div>
        <div>
            <asp:HyperLink ID="hplGoToSecondPage" runat="server" NavigateUrl="~/WebForm2.aspx">Go To the Second Page</asp:HyperLink>
        </div>
    </div>
    </form>
</body>
</html>
