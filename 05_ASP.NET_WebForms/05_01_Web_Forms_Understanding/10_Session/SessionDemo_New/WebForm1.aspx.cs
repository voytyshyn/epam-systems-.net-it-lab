﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SessionDemo_New.Code.Keys;

namespace SessionDemo_New
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
        }

        protected void btnStoreData_Click(object sender, EventArgs e)
        {
            Session[SessionKeys.DATA] = txtSessionData.Text;
        }

        protected void btnRemoveData_Click(object sender, EventArgs e)
        {
            Session.Remove(SessionKeys.DATA);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Session[SessionKeys.DATA] != null)
            {
                txtSessionData.Text = (string)Session[SessionKeys.DATA];
            }
            else
            {
                txtSessionData.Text = "";
            }
        }
    }
}