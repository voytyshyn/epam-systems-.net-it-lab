﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SessionDemo_New.Code.Keys;

namespace SessionDemo_New
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[SessionKeys.DATA] != null)
            {
                lblSessionData.Text = (string)Session[SessionKeys.DATA];
            }
            else
            {
                lblSessionData.Text = "No Data in Session";
            }
        }

        protected void btnAbandonSession_Click(object sender, EventArgs e)
        {
            Session.Abandon();
        }
    }
}