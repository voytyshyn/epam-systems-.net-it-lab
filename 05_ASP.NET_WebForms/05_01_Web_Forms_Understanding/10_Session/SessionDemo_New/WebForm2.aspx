﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="SessionDemo_New.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:Label ID="lblSessionData" runat="server" Text=""></asp:Label>
        </div>
        <div>
            <asp:Button ID="btnAbandonSession" runat="server" Text="Abandon Session" OnClick="btnAbandonSession_Click" />
        </div>
        <div>
            <asp:HyperLink ID="hplGoToFirstPage" runat="server" NavigateUrl="~/WebForm1.aspx">Go To the First Page</asp:HyperLink>
        </div>
    </div>
    </form>
</body>
</html>
