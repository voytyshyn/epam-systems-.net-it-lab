﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_04_PostBack.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Postbacks count: <asp:Literal ID="ltrPostbacks" runat="server" />
        </div>
        <div>
            <asp:CheckBox ID="chkSubmit" runat="server" Text="Click to Submit" AutoPostBack="true" OnCheckedChanged="chkSubmit_CheckedChanged" />
        </div>
        <div>
            <asp:CheckBox ID="chkClick" runat="server" Text="Just Click" OnCheckedChanged="chkClick_CheckedChanged" />
        </div>
        <div>
            Text to Submit: <asp:TextBox ID="txtSubmit" runat="server" AutoPostBack="true" />
        </div>
        <div>
            Just Text: <asp:TextBox ID="txtJustText" runat="server" />
        </div>
        <div>
            <asp:Button ID="btnSubmit" runat="server" Text="Click to Submit" />
        </div>
    </div>
    </form>
</body>
</html>
