﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _04_PostBack
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == true)
            {
                this.PostBackCount += 1;
            }
            ltrPostbacks.Text = this.PostBackCount.ToString();
        }

        private int PostBackCount
        {
            get
            {
                return (ViewState["PostBack"] != null)
                    ? (int)ViewState["PostBack"]
                    : 0;                
            }
            set
            {
                ViewState["PostBack"] = value;
            }
        }

        protected void chkSubmit_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkClick_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}