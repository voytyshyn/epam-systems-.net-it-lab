﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPNETHello
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSayHello_Click(object sender, EventArgs e)
        {
            txtUserName.BorderColor = Color.Empty;
            pnlSayHello.Visible = true;
            pnlEnterName.Visible = false;
            lblSayHello.Text = string.Format("Hello {0}", txtUserName.Text);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnlEnterName.Visible = true;
            pnlSayHello.Visible = false;
            txtUserName.Text = "";
        }
    }
}