﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ASPNETHello.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Panel ID="pnlEnterName" runat="server" Visible="true">
                <asp:Label ID="lblEnterText" runat="server" Text="Enter your name: " />
                <asp:TextBox ID="txtUserName" runat="server" />

                <asp:RequiredFieldValidator ID="rfvUserName" runat="server"
                    ErrorMessage="Required Field" ControlToValidate="txtUserName"
                    ForeColor="Red" Display="Dynamic" />

                <asp:RegularExpressionValidator ID="revUserName" runat="server"
                    ErrorMessage="Length should not exceed 10"  ControlToValidate="txtUserName"
                    ValidationExpression="^[a-zA-Z]{1,10}$"
                    ForeColor="Red" Display="Dynamic" />

                <asp:Button ID="btnSayHello" runat="server" Text="Say Hello" OnClick="btnSayHello_Click" CausesValidation="true" />
            </asp:Panel>

            <asp:Panel ID="pnlSayHello" runat="server" Visible="false">
                <asp:Label ID="lblSayHello" runat="server" />
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
            </asp:Panel>

        </div>
    </form>
</body>
</html>
