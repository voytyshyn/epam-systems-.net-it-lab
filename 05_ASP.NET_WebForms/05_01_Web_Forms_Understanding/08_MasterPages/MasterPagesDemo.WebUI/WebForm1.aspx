﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="MasterPagesDemo.WebUI.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Page #1</title>
    <link href="<%=Page.ResolveUrl("~/Content/Page1.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%=Page.ResolveUrl("~/Scripts/Page1.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h1>The First Page</h1>
    <asp:Button ID="btnGoToTheSecondPage" runat="server" Text="Second Page" OnClick="btnGoToTheSecondPage_Click" OnClientClick="return confirmRedirect();" />
</asp:Content>
