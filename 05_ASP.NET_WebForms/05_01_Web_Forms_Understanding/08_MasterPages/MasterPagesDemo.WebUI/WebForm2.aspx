﻿<%@ Page Title="Another Title" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="MasterPagesDemo.WebUI.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Page #2</title>
    
    <link href="<%=Page.ResolveUrl("~/Content/Page2.css")%>" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h1>The Second Page</h1>
    <asp:Button ID="btnGoToTheFirstPage" runat="server" Text="First Page" OnClick="btnGoToTheFirstPage_Click"/>

</asp:Content>
