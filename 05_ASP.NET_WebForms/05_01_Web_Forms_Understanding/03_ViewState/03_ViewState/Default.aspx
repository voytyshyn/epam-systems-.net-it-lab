﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_03_ViewState.Default" EnableViewState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding:40px;">

        <div>
            <asp:TextBox ID="txtText" runat="server" EnableViewState="false" />
        </div>
        <div>
            <asp:ListBox ID="lstItems" runat="server" Width="200" Height="100" />
        </div>
        <div>
            <asp:Label ID="lblItem" runat="server" />
        </div>
        <div>
            <asp:Button ID="btnButton" runat="server" Text="Click Me" OnClick="btnButton_Click" />
            <asp:Button ID="btnSecondButton" runat="server" Text="Click the Second Button" />
        </div>
        <div>
            <asp:Label ID="lblViewState" runat="server" />
        </div>
 
    </div>
    </form>
</body>
</html>
