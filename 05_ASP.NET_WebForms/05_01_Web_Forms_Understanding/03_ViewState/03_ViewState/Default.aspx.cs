﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace _03_ViewState
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == true)
            {
                string str = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(Request.Form["__VIEWSTATE"]));
                lblViewState.Text = str;
            }

            if (ViewState["item"] != null)
            {
                ViewStateItem item = (ViewStateItem)ViewState["item"];
                lblItem.Text = string.Format("View state item: {0} ({1})", item.Message, item.Time);
            }
            
        }

        protected void btnButton_Click(object sender, EventArgs e)
        {
            txtText.BackColor = Color.Yellow;
            txtText.ForeColor = Color.Blue;

            lstItems.Items.Add("Item #1");
            lstItems.Items.Add("Item #2");
            lstItems.Items.Add("Item #3");
            lstItems.Items.Add("Item #4");
            lstItems.Items.Add("Item #5");

            ViewStateItem item = new ViewStateItem()
            {
                Message = "This is ViewState item",
                Time = DateTime.Now
            };

            ViewState["item"] = item; 
        }
    }
}