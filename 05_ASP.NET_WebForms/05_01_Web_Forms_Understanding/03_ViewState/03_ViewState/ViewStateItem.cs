﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _03_ViewState
{
    [Serializable]
    public class ViewStateItem
    {
        public string Message { get; set; }
        public DateTime Time { get; set; }
    }
}