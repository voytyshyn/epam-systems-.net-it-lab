﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="ThemesDemo.WebUI.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server" SkinID="myTextBoxSkin"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Button" />
        <br />
        <asp:Button ID="Button2" runat="server" ForeColor="Yellow" Text="Button" EnableTheming="false"  />
        <br />
        <asp:DropDownList ID="ddlChangeTheme" runat="server" AutoPostBack="true">
            <asp:ListItem Value="TheFirstTheme">The First Theme</asp:ListItem>
            <asp:ListItem Value="TheSecondTheme">The Second Theme</asp:ListItem>
        </asp:DropDownList>
        <br />
        <div class="my-icon-button"></div>
        <br />
        <div class="my-div">
            My Div
        </div>
    </div>        
    </form>    
</body>
</html>
