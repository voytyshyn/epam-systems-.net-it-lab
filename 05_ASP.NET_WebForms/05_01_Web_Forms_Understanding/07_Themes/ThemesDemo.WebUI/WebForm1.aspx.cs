﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ThemesDemo.WebUI
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                this.Page.Theme = Request["ddlChangeTheme"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlChangeTheme.SelectedValue = this.Page.Theme;
            }
        }
       
    }
}