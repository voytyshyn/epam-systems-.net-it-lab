﻿(function ($) {

    var FractionPage = function () {

        var that = this;

        this.initialize = function () {
            console.log("[fraction-page] initialize", arguments);

            this.$calcButton = $("#calc");
            this.$a1Input = $("#a1");
            this.$b1Input = $("#b1");
            this.$a2Input = $("#a2");
            this.$b2Input = $("#b2");

            this.$aInput = $("#a");
            this.$bInput = $("#b");

            this.$calcButton.on("click", this.onCalc);
        };

        this.onCalc = function () {
            console.log("[fraction-page] onCalc", arguments, this);

            var a1 = that.$a1Input.val();
            var b1 = that.$b1Input.val();
            var a2 = that.$a2Input.val();
            var b2 = that.$b2Input.val();

            $.ajax({
                url: "/Fraction/Calc",
                type: "POST",
                dataType: "json",
                data: {
                    a1: a1,
                    b1: b1,
                    a2: a2,
                    b2: b2
                }
            }).done(function (data) {
                console.log("[fraction-page] onCalc - done", arguments, this);
                that.$aInput.val(data.a);
                that.$bInput.val(data.b);
            });
        };

    };


    $(function () {
        console.log("Fraction page has loaded");
        var page = new FractionPage();
        page.initialize();
    });

})(window.jQuery);