﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _05_Fraction.Controllers
{
    public class FractionController : Controller
    {

        #region Web Actions

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calc(int a1, int b1, int a2, int b2)
        {
            int a = a1 * b2 + a2 * b1;
            int b = b1 * b2;
            return Json(new
            {
                a = a,
                b = b
            });
        }

        #endregion

    }
}