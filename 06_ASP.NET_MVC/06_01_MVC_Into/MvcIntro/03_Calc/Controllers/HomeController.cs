﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _03_Calc.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(double left, string operation, double right)
        {
            double result = left + right;
            ViewBag.Left = left;
            ViewBag.Right = right;
            ViewBag.Result = result;
            return View();
        }
    }
}