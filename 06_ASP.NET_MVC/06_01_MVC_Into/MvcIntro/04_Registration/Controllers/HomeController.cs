﻿using _04_Registration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _04_Registration.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View("Register");
        }

        [HttpPost]
        public ActionResult Index(UserRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                return View("Success");
            }
            else
            {
                return View("Register");
            }
        }
    }
}