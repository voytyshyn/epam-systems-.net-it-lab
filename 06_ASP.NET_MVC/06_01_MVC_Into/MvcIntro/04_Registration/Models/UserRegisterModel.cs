﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Registration.Models
{
    public class UserRegisterModel
    {
        [DisplayName("First Name")]
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        public string FirstName { get; set; }

        [DisplayName("Surname")]
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        public string Surname { get; set; }

        [DisplayName("Email")]
        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")]
        public string Email { get; set; }

        [DisplayName("Password")]
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        public string Password { get; set; }

        [DisplayName("Password Confirmation")]
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        [Compare("Password")]
        public string PasswordConfirmation { get; set; }
    }
}
