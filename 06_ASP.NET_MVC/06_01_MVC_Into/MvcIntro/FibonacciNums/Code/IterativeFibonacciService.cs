﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciNums.Code
{
    public class IterativeFibonacciService : IFibonacciService
    {
        #region IFibonacciService 

        public IEnumerable<KeyValuePair<byte, double>> GetNumbers(byte n)
        {
            #region Validation

            if (n <= 0)
            {
                throw new ArgumentException("n should be greater than 0");
            }

            #endregion

            Dictionary<byte, double> result = new Dictionary<byte, double>();

            if (n >= 1)
            {
                result.Add(1, 1);
            }
            if (n >= 2)
            {
                result.Add(2, 1);            
            }
            if (n >= 3)
            {
                for(byte i = 3; i <= n; i++)
                {
                    double number = result[(byte)(i - 1)] + result[(byte)(i - 2)];
                    result.Add(i, number);
                }
            }

            return result;
        }

        #endregion
    }
}
