﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciNums.Code
{
    public interface IFibonacciService
    {
        /// <summary>
        /// Returns the firsts n Fibonacci numbers
        /// </summary>
        /// <param name="n"></param>
        /// <exception cref="ArgumentException">In case n <= 0</exception>
        /// <returns>The first n Fibonacci numbers</returns>
        IEnumerable<KeyValuePair<byte, double>> GetNumbers(byte n);
    }
}
