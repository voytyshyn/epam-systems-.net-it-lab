﻿using FibonacciNums.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FibonacciNums.Controllers
{
    public class HomeController : Controller
    {

        #region Fields

        private readonly IFibonacciService _fibonacciService;

        #endregion

        #region Constructors

        public HomeController()
        {
            this._fibonacciService = new IterativeFibonacciService();
        }

        #endregion

        #region Web Actions

        // GET: Home
        public ActionResult Index(byte n)
        {
            var numbers = this._fibonacciService.GetNumbers(n);
            ViewBag.Numbers = numbers;
            return View();
        }

        #endregion

        #region Helpers

        #endregion
    }
}