// create a module
var counter = (function () {
    // private field
    var value = 0;

	// private function
	function addValue(n) {
		value += n;
	};
	
	// public object
    return {        
        getValue: function () {
            return value;
        },
		inc : function() {
			addValue(1);
		}
    };
})();