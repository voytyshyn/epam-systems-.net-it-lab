// string helpers lib
var Strings = (function (_self) {

	_self.contains = function (str, subStr) {
        return (str.indexOf(subStr) != -1);
    };

    _self.endsWith = function (str, sufix) {
        var c = str.substring(str.length - sufix.length);
        return (c === sufix);
    };

    _self.startsWith = function (str, prefix) {
        var c = str.substring(0, prefix.length);
        return (c === prefix);
    };
		
    return _self;
	
})(Strings || {});