// Strings lib extention
var Strings = (function (_self) {
	
	// an object that keeps private members
	var _private = {};

	// a private method
	_private.trim = function (str) {
        return str.replace(/^\s+|\s+$/g, "");
    };

	// a public method
    _self.isEmpty = function (text) {
        if ((text === null) || (typeof (text) === 'undifeined')) {
            return true;
        } else {
            var s = (typeof (text) === 'string') ? text : text.toString();
            return _private.trim(text) === "" ? true : false;
        }
    };
    return _self;
	
})(Strings || {});