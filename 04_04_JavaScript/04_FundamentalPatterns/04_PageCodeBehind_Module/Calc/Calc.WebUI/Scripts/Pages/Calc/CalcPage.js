﻿// declare a module appropriate to the "Calc" page
window.CalcPage = (function () {

    // reference to an instance of CalcPage class
    // it's neccessary for defining public members  
    var _public = {};
    // reference to an object keeps private members
    var _private = {};

    // entry point
    _public.initialize = function ($rootElement) {
        _private.$rootElement = $rootElement;
        _private.$calcButton = _private.$rootElement.find("#calc-button");
        _private.$leftArg = _private.$rootElement.find("#left-arg");
        _private.$rightArg = _private.$rootElement.find("#right-arg");
        _private.$operation = _private.$rootElement.find("#operation");
        _private.$result = _private.$rootElement.find("#result");
        
        _private.$calcButton.click(_private.calcButton_Click);
    };

    // private method
    _private.calcButton_Click = function () {
        var leftArg = parseFloat(_private.$leftArg.val());
        var rightArg = parseFloat(_private.$rightArg.val());
        var operation = _private.$operation.val();
        var result = _private.performCalc(leftArg, rightArg, operation);
        _private.$result.text(result);
    };

    // private method
    _private.performCalc = function (leftArg, rightArg, operation) {
        switch (operation) {
            case "+":
                return leftArg + rightArg;
                break;
            case "-":
                return leftArg - rightArg;
                break;
            case "*":
                return leftArg * rightArg;
                break;
            case "/":
                return leftArg / rightArg;
                break;
            default:
                return NaN;
                break;
        }
    };

    // return public object
    return _public;

})();

$(function () {
    // initialize the CalcPage module
    CalcPage.initialize($("#calc-page"));
});