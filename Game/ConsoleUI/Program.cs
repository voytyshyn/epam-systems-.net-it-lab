﻿using System;
using GuessNumber.GameEngine;

namespace GuessNumber.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game(5);
            game.StartNewGame(2, 15);
            //game.TryGuessNumber_Old();

            RunGame(game);
        }

        private static void RunGame(Game game)
        {
            var number = game.DefineNumber();
            Console.WriteLine("Player 1 generated number: {0}", number);
            Console.ReadKey();

            do
            {
                var assumedNumber = game.TryGuessNumber();

                Console.WriteLine("Player 2 say: {0}", assumedNumber);

                var result = game.LeftPlayerAnwer();
                Console.WriteLine("LeftPlayer answered: {0}", result < 0 ? "<" : ">");

                game.RightPlayerAnalyzeAnswer(result);

                Console.ReadKey();

            } while (!game.IsExit());

            Console.WriteLine(!game.IsNumberWasGuessed() ? "You are looser((((" : "You win!!!!");
        }

    }
}
