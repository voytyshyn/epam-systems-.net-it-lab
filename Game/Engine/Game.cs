﻿using System;
using GuessNumber.GameEngine.Players;

namespace GuessNumber.GameEngine
{
    public class Game
    {
        private int _maxCount;
        
        private LeftPlayer _leftPlayer;
        private RightPlayer _rightPlayer;

        public int AttemptCount { get; private set; }

        public Game(int maxCount)
        {
            this._maxCount = maxCount;
        }

        public void StartNewGame(int leftBorder, int rightBorder)
        {
            _leftPlayer = new LeftPlayer(leftBorder, rightBorder);
            _rightPlayer = new RightPlayer(leftBorder, rightBorder);
        }

        public int DefineNumber()
        {
            return _leftPlayer.Number;
        }

        public int TryGuessNumber()
        {
            AttemptCount ++;
            return _rightPlayer.GenerateAnswer();
        }

        public int LeftPlayerAnwer()
        {
            return _leftPlayer.CompareAnswer(_rightPlayer.Number);
        }

        public void RightPlayerAnalyzeAnswer(int result)
        {
            _rightPlayer.AnalyzeAnswer(result);
        }

        public bool IsExit()
        {
            return _leftPlayer.Number == _rightPlayer.Number || AttemptCount >= _maxCount;
        }

        public bool IsNumberWasGuessed()
        {
            return _leftPlayer.Number == _rightPlayer.Number;
        }

        public void TryGuessNumber_Old()
        {
            var number = _leftPlayer.Number;
            Console.WriteLine("Player 1 generated number: {0}", number);
            Console.ReadKey();

            var assumedNumber = 0;
            do
            {
                assumedNumber = _rightPlayer.GenerateAnswer();

                Console.WriteLine("Player 2 say: {0}", assumedNumber);

                var result = _leftPlayer.CompareAnswer(assumedNumber);
                _rightPlayer.AnalyzeAnswer(result);

                AttemptCount ++;

                Console.ReadKey();

            } while (assumedNumber != number && AttemptCount < _maxCount);

            Console.WriteLine(assumedNumber != number ? "You are looser((((" : "You win!!!!");
        }


    }
}
