﻿using System;
using GuessNumber.GameEngine.Players.Base;
using GuessNumber.GameEngine.Players.Interfaces;

namespace GuessNumber.GameEngine.Players
{
    public class LeftPlayer : PlayerBase, ILeftPlayer
    {
        public LeftPlayer(int leftBorder, int rightBorder) : base(leftBorder, rightBorder)
        {
            var rnd = new Random((int)DateTime.Now.TimeOfDay.TotalMilliseconds);
            Number = rnd.Next(leftBorder, rightBorder + 1);
        }

        public int CompareAnswer(int answer)
        {
            if (answer > Number)
            {
                return 1;
            }
            else if (answer < Number)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
