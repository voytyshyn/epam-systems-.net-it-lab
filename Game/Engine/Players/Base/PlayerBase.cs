﻿
namespace GuessNumber.GameEngine.Players.Base
{
    public class PlayerBase
    {
        public int LeftBorder { get; protected set; }

        public int RightBorder { get; protected set; }

        public int Number { get; protected set; }

        public PlayerBase(int leftBorder, int rightBorder)
        {
            this.LeftBorder = leftBorder;
            this.RightBorder = rightBorder;
        }

        
    }
}
