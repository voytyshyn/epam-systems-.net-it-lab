﻿using System;
using GuessNumber.GameEngine.Players.Base;
using GuessNumber.GameEngine.Players.Interfaces;

namespace GuessNumber.GameEngine.Players
{
    public class RightPlayer : PlayerBase, IRightPlayer
    {
        public RightPlayer(int leftBorder, int rightBorder): base(leftBorder, rightBorder)
        {
        }

        public void AnalyzeAnswer(int result)
        {
            if (result < 0)
            {
                this.LeftBorder = Number + 1;
            }
            else
            {
                this.RightBorder = Number - 1;
            }
        }

        public int GenerateAnswer()
        {
            var rnd = new Random((int) (DateTime.Now.TimeOfDay.TotalMilliseconds*111.111));
            Number = rnd.Next(LeftBorder, RightBorder + 1);
            return Number;
        }
    }
}
