﻿
namespace GuessNumber.GameEngine.Players.Interfaces
{
    public interface ILeftPlayer
    {
        int CompareAnswer(int answer);
    }
}
