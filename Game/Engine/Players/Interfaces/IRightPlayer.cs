﻿namespace GuessNumber.GameEngine.Players.Interfaces
{
    public interface IRightPlayer
    {
        void AnalyzeAnswer(int result);
        int GenerateAnswer();
    }
}