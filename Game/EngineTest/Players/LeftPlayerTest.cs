﻿using GuessNumber.GameEngine.Players;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GuessNumber.GameEngine.Test.Players
{
    [TestClass]
    public class LeftPlayerTest
    {
        [TestMethod]
        public void TestGenerateNumber()
        {
            var leftBorder = 0;
            var rightBorder = 10;
            var player = new LeftPlayer(leftBorder, rightBorder);
            var number = player.Number;

            Assert.IsTrue(number >= leftBorder);
            Assert.IsTrue(number <= rightBorder);
        }
    }
}
