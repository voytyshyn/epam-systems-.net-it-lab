﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GuessNumber.GameEngine.Test
{
    [TestClass]
    public class GameTest
    {
        [TestMethod]
        public void TestTryGuessNumber_CheckAttemptCount()
        {
            var game = new Game(5);
            game.StartNewGame(4, 12);

            var number = game.DefineNumber();
            var assumedNumber = game.TryGuessNumber();

            Assert.AreEqual(1, game.AttemptCount);
        }
    }
}
