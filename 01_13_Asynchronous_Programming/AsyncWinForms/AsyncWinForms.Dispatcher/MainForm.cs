﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsyncWinForms.Dispatcher
{
    public partial class MainForm : Form
    {
        private byte[] _array;
        private long _sum;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _calcellationToken;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
            this.InitArray();
        }

        private void btnCalcSum_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(this.CalcSum);
            this._cancellationTokenSource = new CancellationTokenSource();
            this._calcellationToken = this._cancellationTokenSource.Token;
            t.IsBackground = true;
            t.Start();
            //this.CalcSum();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this._cancellationTokenSource.Cancel();
        }

        private void InitArray()
        {
            this._array = new byte[1000000];
            for (int i = 0; i < this._array.Length; i++)
            {
                this._array[i] = 1;
            }
        }

        private void CalcSum()
        {
            this._sum = 0;
            Action d = this.UpdateSum;
            for (int i = 0; i < this._array.Length; i++)
            {
                if (this._calcellationToken.IsCancellationRequested)
                {
                    return;
                }
                this._sum += this._array[i];
                this.Invoke(d);
            }            
        }

        private void UpdateSum()
        {
            lblSum.Text = this._sum.ToString();
        }



    }
}
