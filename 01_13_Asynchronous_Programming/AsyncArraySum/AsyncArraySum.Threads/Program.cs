﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncArraySum.Threads
{
    class Program
    {
        private static long sum;

        static void Main(string[] args)
        {
            byte[] array = new byte[10000000];
            for(int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }

            Thread t = new Thread(CalcSum);
            //t.IsBackground = true;
            t.Start(array);
            //t.Join();
            while(t.IsAlive)
            {
                Console.Write(".");
                Thread.Sleep(100);
            }
            Console.WriteLine();
            Console.WriteLine("Sum = {0}", sum);
        }
        
        private static void CalcSum(object obj)
        {
            byte[] numbers = (byte[])obj;
            long s = 0;

            foreach (var n in numbers)
            {
                s += n;
                Thread.Yield();
            }

            sum = s;
        }
    }
}
