﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncArraySum.ThreadPoolUsing
{
    class Program
    {

        static void Main(string[] args)
        {
            byte[] array = new byte[10000000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }

            ThreadPool.QueueUserWorkItem(CalcSum, array);
            
            Thread.Sleep(2000);
        }

        private static void CalcSum(object obj)
        {
            Console.WriteLine("Sum calc has started...");
            byte[] numbers = (byte[])obj;
            long s = 0;

            foreach (var n in numbers)
            {
                s += n;
                Thread.Yield();
            }

            Console.WriteLine("Sum = {0}", s);
        }
    }
}
