﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncArraySum.AsyncDelegateUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] array = new byte[10000000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }

            Func<byte[], long> d = CalcSum;

            IAsyncResult ar = d.BeginInvoke(array, null, null);

            long sum = d.EndInvoke(ar);
            Console.WriteLine("Sum = {0}", sum);

        }

        private static long CalcSum(byte[] numbers)
        {
            
            long s = 0;

            foreach (var n in numbers)
            {
                s += n;
                Thread.Yield();
            }

            return s;
        }
    }
}
