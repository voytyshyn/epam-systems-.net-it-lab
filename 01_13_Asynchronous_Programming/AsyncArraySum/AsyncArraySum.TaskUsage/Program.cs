﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncArraySum.TaskUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] array = new byte[10000000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }


            Task<long> task = new Task<long>(CalcSum, array);
            task.Start();
            long sum = task.Result;
            Console.WriteLine("Sum = {0}", sum);

        }

        private static long CalcSum(object obj)
        {
            byte[] numbers = (byte[])obj;
            long s = 0;

            foreach (var n in numbers)
            {
                s += n;
                Thread.Yield();
            }

            return s;
        }
    }
}
