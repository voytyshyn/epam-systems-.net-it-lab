﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncArraySum.ParallelUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            Parallel.For(0, 10, Print);
        }

        static void Print(int i)
        {
            Console.WriteLine(i);
        }
    }
}
