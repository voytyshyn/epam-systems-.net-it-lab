﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Picker.GameEngine.Test
{
    [TestClass]
    public class GameTest
    {

        #region Game Lifecycle

        [TestMethod]
        public void TestUsualLifecycle()
        {
            Game game = new Game();
            Assert.AreEqual(GameStatus.ReadyToStart, game.Status);
            game.Start();
            Assert.AreEqual(GameStatus.InProgress, game.Status);
            game.Stop();
            Assert.AreEqual(GameStatus.Completed, game.Status);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestStart_WrongStatus_1()
        {
            Game game = new Game();            
            game.Start();            
            game.Start();                    
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestStart_WrongStatus_2()
        {
            Game game = new Game();
            game.Start();
            game.Stop();
            game.Start();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestStop_WrongStatus_1()
        {
            Game game = new Game();
            game.Stop();            
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestStop_WrongStatus_2()
        {
            Game game = new Game();
            game.Start();
            game.Stop();
            game.Stop();
        }


        #endregion

        #region Board

        [TestMethod]
        public void TestBoardCreation()
        {
            Game game = new Game();
            Assert.IsNotNull(game.Board);
        }

        [TestMethod]
        public void TestBoardSize()
        {
            Game game = new Game();
            Assert.AreEqual(25, game.Board.Width);
            Assert.AreEqual(10, game.Board.Height);
        }

        #endregion

        #region Player

        [TestMethod]
        public void TestPlayerCreation()
        {
            Game game = new Game();
            Assert.IsNotNull(game.Player);
            Assert.AreSame(game.Board, game.Player.Board);
            Assert.IsTrue(game.Board.GameObjects.Any(r => r == game.Player));
        }

        #endregion
    }
}
