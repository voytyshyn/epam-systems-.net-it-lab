﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Picker.GameEngine.GameObjects;

namespace Picker.GameEngine.Test.GameObjects
{
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Player player = new Player(1, 2);
            Assert.AreEqual(1, player.X);
            Assert.AreEqual(2, player.Y);
            Assert.IsNull(player.Board);
        }

        [TestMethod]
        public void TestCanBeMovedOn_Without_Board()
        {
            Player player = new Player(1, 2);
            Assert.IsTrue(player.CanBeMovedOn(-1, -1));
        }

        [TestMethod]
        public void TestCanBeMovedOn_With_Board()
        {
            Player player = new Player(1, 2);
            GameBoard board = new GameBoard(10, 7);
            board.AddGameObject(player);

            Assert.IsTrue(player.CanBeMovedOn(-1, -1));
            Assert.IsTrue(player.CanBeMovedOn(1, 1));
            Assert.IsTrue(player.CanBeMovedOn(8, 4));
            Assert.IsTrue(player.CanBeMovedOn(-1, -2));

            Assert.IsFalse(player.CanBeMovedOn(9, -1));
            Assert.IsFalse(player.CanBeMovedOn(-2, -1));
            Assert.IsFalse(player.CanBeMovedOn(0, 5));
            Assert.IsFalse(player.CanBeMovedOn(0, -3));
        }

        [TestMethod]
        public void TestMoveOn_Without_Board()
        {
            Player player = new Player(1, 2);
            player.MoveOn(1, -1);
            Assert.AreEqual(2, player.X);
            Assert.AreEqual(1, player.Y);
        }        

        [TestMethod]
        public void TestMoveOn_With_Board()
        {
            Player player = new Player(1, 2);
            GameBoard board = new GameBoard(10, 7);
            board.AddGameObject(player);
            player.MoveOn(4, -2);
            Assert.AreEqual(5, player.X);
            Assert.AreEqual(0, player.Y);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMoveOn_With_Board_WrongX()
        {
            Player player = new Player(1, 2);
            GameBoard board = new GameBoard(10, 7);
            board.AddGameObject(player);
            player.MoveOn(9, -2);            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMoveOn_With_Board_WrongY()
        {
            Player player = new Player(1, 2);
            GameBoard board = new GameBoard(10, 7);
            board.AddGameObject(player);
            player.MoveOn(2, -3);
        }
    }
}
