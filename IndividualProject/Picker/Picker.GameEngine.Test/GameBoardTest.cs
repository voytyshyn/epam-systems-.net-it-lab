﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Picker.GameEngine.GameObjects;

namespace Picker.GameEngine.Test
{
    [TestClass]
    public class GameBoardTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            GameBoard board = new GameBoard(10, 8);
            Assert.AreEqual(10, board.Width);
            Assert.AreEqual(8, board.Height);
            Assert.IsNotNull(board.GameObjects);
            Assert.AreEqual(0, board.GameObjects.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestWidth_Wrong_0()
        {
            GameBoard board = new GameBoard(0, 8);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestWidth_Wrong_1()
        {
            GameBoard board = new GameBoard(-2, 8);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestHeight_Wrong_0()
        {
            GameBoard board = new GameBoard(5, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestHeight_Wrong_1()
        {
            GameBoard board = new GameBoard(5, -2);
        }

        [TestMethod]
        public void TestAddGameObject_Player()
        {
            GameBoard board = new GameBoard(10, 5);
            IGameObject player = new Player(1, 1);
            board.AddGameObject(player);

            IGameObject lastGameObject = board.GameObjects.Last();
            Assert.AreSame(lastGameObject, player);
            Assert.AreSame(board, player.Board);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestAddGameObject_Player_Wrong_Position()
        {
            GameBoard board = new GameBoard(10, 5);
            IGameObject player = new Player(11, 1);
            board.AddGameObject(player);
        }

        [TestMethod]
        public void TestCanBePlaced_Player()
        {
            GameBoard board = new GameBoard(10, 5);
            IGameObject player = new Player(1, 1);
            board.AddGameObject(player);

            Assert.IsTrue(board.CanBePlaced(player, 0, 0));
            Assert.IsTrue(board.CanBePlaced(player, 9, 4));
            Assert.IsTrue(board.CanBePlaced(player, 2, 1));

            Assert.IsFalse(board.CanBePlaced(player, -1, 0));
            Assert.IsFalse(board.CanBePlaced(player, 0, -1));
            Assert.IsFalse(board.CanBePlaced(player, 10, 0));
            Assert.IsFalse(board.CanBePlaced(player, 0, 5));
        }

        [TestMethod]
        public void TestCanBePlaced_Player_Does_Not_Belong_To()
        {
            GameBoard board = new GameBoard(10, 5);
            IGameObject player = new Player(1, 1);
            Assert.IsFalse(board.CanBePlaced(player, 0, 5));
        }
    }
}
