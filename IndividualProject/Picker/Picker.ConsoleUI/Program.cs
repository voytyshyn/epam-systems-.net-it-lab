﻿using Picker.GameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.ConsoleUI
{
    class Program
    {        

        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Game game = new Game();
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    game.Start();                    
                    //Console.WriteLine("The game has been started");
                    DrawBoard(game);
                    DrawPlayer(game);
                }
                else if (key.Key == ConsoleKey.LeftArrow)
                {
                    if (game.Player.CanBeMovedOn(-1, 0))
                    {
                        HidePlayer(game);
                        game.Player.MoveOn(-1, 0);
                        DrawPlayer(game);
                    }
                }
                else if (key.Key == ConsoleKey.RightArrow)
                {
                    if (game.Player.CanBeMovedOn(1, 0))
                    {
                        HidePlayer(game);
                        game.Player.MoveOn(1, 0);
                        DrawPlayer(game);
                    }
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    if (game.Player.CanBeMovedOn(0, -1))
                    {
                        HidePlayer(game);
                        game.Player.MoveOn(0, -1);
                        DrawPlayer(game);
                    }
                }
                else if (key.Key == ConsoleKey.DownArrow)
                {
                    if (game.Player.CanBeMovedOn(0, 1))
                    {
                        HidePlayer(game);
                        game.Player.MoveOn(0, 1);
                        DrawPlayer(game);
                    }
                }
                else if (key.Key == ConsoleKey.Q || key.Key == ConsoleKey.Escape)
                {                    
                    game.Stop();
                    break;
                }
            }
            Console.SetCursorPosition(0, 2 + 2 * game.Board.Height);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void DrawBoard(Game game)
        {
            int w = 2 * game.Board.Width;
            int h = 2 * game.Board.Height;

            Console.ForegroundColor = ConsoleColor.White;

            Console.SetCursorPosition(0, 0);
            Console.Write("╔");
            Console.SetCursorPosition(1, 0);
            Console.Write(new string('═', w));
            Console.SetCursorPosition(w + 1, 0);
            Console.Write("╗");

            Console.SetCursorPosition(0, h + 1);
            Console.Write("╚");
            Console.SetCursorPosition(1, h + 1);
            Console.Write(new string('═', w));
            Console.SetCursorPosition(w + 1, h + 1);
            Console.Write("╝");

            for (int i = 0; i < h; i++)
            {
                Console.CursorTop = i + 1;
                Console.CursorLeft = 0;
                Console.Write("║");
                Console.CursorLeft = w + 1;
                Console.Write("║");
            }
            
        }

        private static void HidePlayer(Game game)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(2 * game.Player.X + 1, 2 * game.Player.Y + 1);            
            Console.Write("  ");
            Console.SetCursorPosition(2 * game.Player.X + 1, 2 * game.Player.Y + 2);
            Console.Write("  ");
        }

        private static void DrawPlayer(Game game)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(2 * game.Player.X + 1, 2 * game.Player.Y + 1);
            Console.Write("**");
            Console.SetCursorPosition(2 * game.Player.X + 1, 2 * game.Player.Y + 2);
            Console.Write("**");
        }
    }
}
