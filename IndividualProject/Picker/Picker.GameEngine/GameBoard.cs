﻿using Picker.GameEngine.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine
{
    public class GameBoard
    {
        #region Fields

        private List<IGameObject> _gameObjects = new List<IGameObject>();

        #endregion

        #region Constructors

        public GameBoard(int width, int height)
        {
            #region Validation

            if (width <= 0)
            {
                throw new ArgumentException("'width' cannot be <= 0");
            }

            if (height <= 0)
            {
                throw new ArgumentException("'height' cannot be <= 0");
            }

            #endregion

            this.Width = width;
            this.Height = height;
        }

        #endregion

        #region Public Properties

        public int Width { get; private set; }
        public int Height { get; private set; }

        public IEnumerable<IGameObject> GameObjects
        {
            get { return this._gameObjects; }
        }

        #endregion

        #region Public Methods

        public void AddGameObject(IGameObject gameObject)
        {
            if (this.IsCorrectPosition(gameObject, gameObject.X, gameObject.Y) == false)
            {
                throw new ArgumentException("A game object cannot be added to the board due to incorrect coordinates");
            }
            this._gameObjects.Add(gameObject);
            gameObject.Board = this;
        }

        public bool CanBePlaced(IGameObject gameObject, int newX, int newY)
        {
            if (this._gameObjects.Any(r => r == gameObject) == false)
            {
                return false;
            }

            return IsCorrectPosition(gameObject, newX, newY);
        }        

        #endregion

        #region Helpers

        private bool IsCorrectPosition(IGameObject gameObject, int newX, int newY)
        {
            if (gameObject is Player)
            {
                return ((newX >= 0) && (newX < this.Width) && (newY >= 0) && (newY < this.Height));
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
