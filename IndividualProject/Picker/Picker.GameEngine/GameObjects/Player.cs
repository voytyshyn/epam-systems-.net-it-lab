﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine.GameObjects
{
    public class Player: BaseGameObject, IMovable
    {
        public Player(int x, int y): base(x, y)
        {
        }

        public void MoveOn(int dx, int dy)
        {
            if (this.CanBeMovedOn(dx, dy) == false)
            {
                throw new ArgumentException("Player cannot be moved to the specified positon");
            }

            this.X += dx;
            this.Y += dy;
        }

        public bool CanBeMovedOn(int dx, int dy)
        {
            int newX = this.X + dx;
            int newY = this.Y + dy;
            if (this.Board != null)
            {
                return this.Board.CanBePlaced(this, newX, newY);
            }
            else
            {
                return true;
            }
        }
    }
}