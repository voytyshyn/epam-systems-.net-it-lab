﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine.GameObjects
{
    public interface IMovable
    {
        void MoveOn(int dx, int dy);
    }
}
