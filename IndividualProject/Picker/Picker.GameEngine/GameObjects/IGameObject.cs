﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine.GameObjects
{
    public interface IGameObject
    {
        GameBoard Board { get; set; }
        int X { get; }
        int Y { get; }
    }
}
