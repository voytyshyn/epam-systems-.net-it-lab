﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine.GameObjects
{
    public abstract class BaseGameObject: IGameObject
    {
        #region Fields

        private GameBoard _board = null;
        private int _x;
        private int _y;

        #endregion

        #region Constructors

        public BaseGameObject(int x, int y)
        {
            this._x = x;
            this._y = y; 
        }

        #endregion

        #region IGameObject Members

        public GameBoard Board
        {
            get
            {
                return this._board;
            }
            set
            {
                this._board = value;                
            }
        }

        public int X
        {
            get { return this._x; }
            protected set { this._x = value; }
        }

        public int Y
        {
            get { return this._y; }
            protected set { this._y = value; }
        }

        #endregion
    }
}
