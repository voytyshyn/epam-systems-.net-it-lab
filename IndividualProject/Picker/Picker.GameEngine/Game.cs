﻿using Picker.GameEngine.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picker.GameEngine
{
    public enum GameStatus
    {
        ReadyToStart,
        InProgress,
        Completed
    }

    public class Game
    {
        #region Private Fields

        private GameStatus _status;
        private Player _player;
        private GameBoard _board;

        #endregion

        #region Constructors

        public Game()
        {
            this._status = GameStatus.ReadyToStart;
            this._board = new GameBoard(25, 10);
            this._player = new Player(1, 1);
            this._board.AddGameObject(this._player);
        }

        #endregion

        #region Public Properties

        public GameStatus Status
        {
            get { return this._status; }
        }

        public GameBoard Board
        {
            get { return this._board; }
        }

        public Player Player
        {
            get { return this._player; }
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            #region Validation

            if (this._status != GameStatus.ReadyToStart)
            {
                throw new InvalidOperationException("Only game with status 'ReadyToStart' can be started");
            }

            #endregion

            this._status = GameStatus.InProgress;
        }

        public void Stop()
        {
            #region Validation

            if (this._status != GameStatus.InProgress)
            {
                throw new InvalidOperationException("Only game with status 'InProgress' can be stopped");
            }

            #endregion

            this._status = GameStatus.Completed;
        }

        #endregion

        #region Helpers

        #endregion

    }
}